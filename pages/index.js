import { Box, Heading } from 'rebass/styled-components'

import Layout from 'components/Layout'

const Publication = (props) => (
  <Box {...props} sx={{
    width: '100%',
    height: 200,
    mb: 3,
    p: 3,
    bg: 'white',
    borderRadius: 5,
    boxShadow: 'small',
    color: 'primary',
    ...props.sx
  }} />
)

const Widget = (props) => (
  <Box {...props} sx={{
    width: '100%',
    height: 300,
    mb: 3,
    p: 3,
    bg: 'white',
    borderRadius: 5,
    boxShadow: 'small',
    color: 'primary',
    ...props.sx
  }} />
)

const AsideContent = () => (
  <Box>
    <Widget>Widget #1</Widget>
    <Widget>Widget #2</Widget>
  </Box>
)

const IndexPage = () => (
  <Layout aside={<AsideContent />}>
    <Box>
      <Publication>Publication #1</Publication>
      <Publication>Publication #2</Publication>
      <Publication>Publication #3</Publication>
      <Publication>Publication #4</Publication>
      <Publication>Publication #5</Publication>
      <Publication>Publication #6</Publication>
      <Publication>Publication #7</Publication>
      <Publication>Publication #8</Publication>
    </Box>
  </Layout>
)

export default IndexPage