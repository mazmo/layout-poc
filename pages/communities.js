import { Box, Heading } from 'rebass/styled-components'

import Layout from 'components/Layout'

const Thread = (props) => (
  <Box {...props} sx={{
    width: '100%',
    height: 200,
    mb: 3,
    p: 3,
    bg: 'white',
    borderRadius: 5,
    boxShadow: 'small',
    color: 'primary',
    ...props.sx
  }} />
)

const Widget = (props) => (
  <Box {...props} sx={{
    width: '100%',
    height: 300,
    mb: 3,
    p: 3,
    bg: 'white',
    borderRadius: 5,
    boxShadow: 'small',
    color: 'primary',
    ...props.sx
  }} />
)

const AsideContent = () => (
  <Box>
    <Widget>Widget #1</Widget>
    <Widget>Widget #2</Widget>
  </Box>
)

const CommunitiesPage = () => (
  <Layout aside={<AsideContent />}>
    <Box>
      <Thread>Thread #1</Thread>
      <Thread>Thread #2</Thread>
      <Thread>Thread #3</Thread>
      <Thread>Thread #4</Thread>
      <Thread>Thread #5</Thread>
      <Thread>Thread #6</Thread>
      <Thread>Thread #7</Thread>
      <Thread>Thread #8</Thread>
    </Box>
  </Layout>
)

export default CommunitiesPage