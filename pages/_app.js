import Head from 'next/head'
import { ThemeProvider } from 'styled-components'
import { Provider } from 'react-redux'

import GlobalStyles from 'components/GlobalStyles'

import { useStore } from 'redux/store'
import theme from 'styles/theme'

const App = ({ Component, pageProps }) => {
  const store = useStore(pageProps.initialReduxState)

  return (
    <ThemeProvider theme={theme}>
      <Head>
        <title>Mazmo</title>
        <meta property="og:title" content="Mazmo" key="title" />
      </Head>
      <Provider store={store}>
        <GlobalStyles />
        <Component {...pageProps} />
      </Provider>
    </ThemeProvider>
  )
}

export default App