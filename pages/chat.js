import { Box, Heading } from 'rebass/styled-components'

import Nav from 'components/Nav'
import Pre from 'components/Pre'

const ChatPage = () => {
  return (
    <Box>
      <Nav />
      <Box px={4}>
        <Heading as="h1" my={3} fontSize={6}>Chat</Heading>
        <Pre data={{ a: 1 }} />
      </Box>
    </Box>
  )
}

export default ChatPage