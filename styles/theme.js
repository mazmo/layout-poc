const primary = '#ED4D3D'

export default {
  breakpoints: ['52em', '64em', '90em'],
  fontSizes: [
    12, 14, 16, 20, 24, 32, 48, 64
  ],
  colors: {
    primary: primary,
    lightgray: '#f6f6f6',
    text: {
      primary: '#ffffff',
      accent: primary
    },
    background: {
      primary: '#ffffff',
      secondary: '#f0f0f0',
      accent: primary
    }
  },
  space: [
    0, 4, 8, 16, 32, 64, 128, 256
  ],
  fonts: {
    body: 'Roboto, system-ui, sans-serif',
    heading: '"Titillium Web"',
    sources: [
      'https://fonts.googleapis.com/css2?family=Roboto:wght@300,400,500,700&display=swap',
      'https://fonts.googleapis.com/css2?family=Titillium+Web:wght@300;400;600;700&display=swap'
    ]
  },
  fontWeights: {
    body: 400,
    heading: 700,
    bold: 700,
  },
  lineHeights: {
    body: 1.5,
    heading: 1.25,
  },
  shadows: {
    small: '0 1px 5px 1px rgba(0, 0, 0, .1)',
    large: '0 1px 10px 2px rgba(0, 0, 0, .1)'
  },
  variants: {
  },
  text: {
  },
  buttons: {
    primary: {
      color: 'white',
      bg: 'primary',
    }
  }
}