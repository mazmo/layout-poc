import { useRouter } from 'next/router'
import NextLink from 'next/link'
import useMobileDetect from 'use-mobile-detect-hook'

import { Box, Link } from 'rebass/styled-components'

const HeaderLink = React.forwardRef((props, ref) =>
  <Link
    {...props}
    ref={ref}
    sx={{
      display: 'block',
      px: 3,
      fontWeight: 'heading',
      fontSize: 2,
      bg: props.active ? 'rgba(255, 255, 255, .25)' : 'transparent',
      color: 'text.primary',
      textDecoration: 'none',
      textTransform: 'uppercase',
      lineHeight: '50px',
      '&:hover': {
        backgroundColor: 'rgba(255, 255, 255, 0.1)'
      }
    }}    
  />
)

const Nav = (props) => {
  const detectMobile = useMobileDetect()
  const router = useRouter()

  return (
    <Box
      {...props}
      sx={{
        position: 'fixed',
        zIndex: 8,
        bottom: 0,
        left: 0,
        width: '100%',
        height: 'calc(100% - 54px)',
        transform: `translateX(${props.visible ? '0px' : '-250px'})`,
        pointerEvents: detectMobile.isDesktop() ? 'none' : 'all',
        bg: props.visible ? ['rgba(0, 0, 0, .25)', 'rgba(0, 0, 0, .25)', 'transparent'] : 'transparent',
        transition: 'transform linear 100ms, background-color linear 100 150ms',
        ...props.sx
      }}
      onClick={props.overlayClick}
    >
      <Box sx={{ width: 250, height: '100%', bg: '#303030', pointerEvents: 'all' }} onClick={(event) => event.stopPropagation()}>
        <NextLink href="/" passHref>
          <HeaderLink active={router.asPath === '/'}>Inicio</HeaderLink>
        </NextLink>
        <NextLink href="/communities" passHref>
          <HeaderLink active={router.asPath.startsWith('/communities')}>Communities</HeaderLink>
        </NextLink>
        <NextLink href="/chat" passHref>
          <HeaderLink active={router.asPath.startsWith('/chat')}>Chat</HeaderLink>
        </NextLink>
        <NextLink href="/events" passHref>
          <HeaderLink active={router.asPath.startsWith('/events')}>Events</HeaderLink>
        </NextLink>
      </Box>
    </Box>
  )
}

export default Nav