import { createGlobalStyle } from 'styled-components'

const GlobalStyles = createGlobalStyle`
  ${props => props.theme.fonts.sources.map(source => `@import url('${source}');`)}

  *,
  *::before,
  *::after {
    box-sizing: border-box;
  }

  html {
    -webkit-font-smoothing: antialiased;
    text-rendering: optimizeLegibility;
  }

  html,
  body,
  #__next {
    height: 100%;
  }

  body {
    margin: 0;
    font-family: ${props => props.theme.fonts.body};
    background-color: ${props => props.theme.colors.background.secondary};
    color: ${props => props.theme.colors.text.primary};
  }

  h1 {
    font-family: ${props => props.theme.fonts.heading};
  }
`

export default GlobalStyles