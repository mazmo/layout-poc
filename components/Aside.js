import { Flex, Box } from 'rebass/styled-components'
import useWindowDimensions from 'hooks/useWindowDimensions'

const Aside = (props) => {
  const { width } = useWindowDimensions()

  return (
    <Flex
      {...props}
      sx={{
        position: ['fixed', 'fixed', 'sticky'],
        zIndex: 8,
        top: [54, 54, 70],
        right: 0,
        width: ['100%', '100%', 300],
        height: 'calc(100% - 54px)',
        justifyContent: 'flex-end',
        ml: 3,
        p: 0,
        transform: `translateX(${props.visible ? '0' : '100%'})`,
        bg: props.visible ? ['rgba(0, 0, 0, .25)', 'rgba(0, 0, 0, .25)', 'transparent'] : 'transparent',
        transition: 'transform linear 100ms, background-color linear 100 150ms',
        willChange: 'transform',
        ...props.sx
      }}
      onClick={props.overlayClick}
    >
      <Box sx={{ width: 300, px: 3, py: [3, 3, 0] }} onClick={(event) => event.stopPropagation()}>
        {props.children}
      </Box>
    </Flex>
  )
}

export default Aside