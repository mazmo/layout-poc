import { Box } from 'rebass/styled-components'

const Container = (props) => (
  <Box
    {...props}
    sx={{
      width: '100%',
      maxWidth: ['100%', 768, 960],
      mx: 'auto',
      ...props.sx
    }} />
)

export default Container