import React from 'react'
import { Flex, Box } from 'rebass/styled-components'
import { useSwipeable } from 'react-swipeable'
import useWindowDimensions from 'hooks/useWindowDimensions'

import Container from 'components/Container'
import Header from 'components/Header'
import Nav from 'components/Nav'
import Aside from 'components/Aside'

const Layout = (props) => {
  const { width } = useWindowDimensions();
  console.log(width >= 900)
  const [navVisible, setNavVisible] = React.useState(false)
  const [asideVisible, setAsideVisible] = React.useState(false)
  const handlers = useSwipeable({
    onSwipedRight: (eventData) => {
      if (width < 1024) {
        if (!navVisible && !asideVisible) setNavVisible(true)
        if (asideVisible) setAsideVisible(false)
      }
    },
    onSwipedLeft: (eventData) => {
      if (width < 1024) {
        if (!navVisible && !asideVisible) setAsideVisible(true)
        if (navVisible) setNavVisible(false)
      }
    }
  })

  const showNav = () => {
    setNavVisible(true)
    hideAside()
  }

  const hideNav = () => {
    setNavVisible(false)
  }

  const showAside = () => {
    setAsideVisible(true)
    hideNav()
  }

  const hideAside = () => {
    setAsideVisible(width >= 1024)
    setAsideVisible(width >= 1024)
  }

  React.useEffect(() => {
    setNavVisible(width >= 1300)
    setAsideVisible(width >= 1024)
  }, [width]);

  return (
    <Box {...handlers} sx={{ pt: 54 }}>
      <Header
        onNavOpenClick={showNav}
        onNavCloseClick={hideNav}
        onAsideCloseClick={hideAside}
        navVisible={navVisible}
        asideVisible={asideVisible}
      />
      <Nav visible={navVisible} overlayClick={hideNav} />
      <Box sx={{ width: '100%', pl: width > 1300 && navVisible ? 250 : 0 }}>
        <Container sx={{ mt: 3, px: [3, 0] }}>
          <Flex>
            <Flex sx={{ flexGrow: 1, flexDirection: 'column' }}>
              {props.children}
            </Flex>
            {props.aside && <Aside visible={asideVisible} overlayClick={hideAside}>{props.aside}</Aside>}
          </Flex>
        </Container>
      </Box>
    </Box>
  )
}

export default Layout