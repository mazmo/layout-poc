import { Box, Flex, Link, Image } from 'rebass/styled-components'
import { Input } from '@rebass/forms'
import MenuIcon from '@material-ui/icons/Menu'
import MenuOpenIcon from '@material-ui/icons/MenuOpen'
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft'
import AddIcon from '@material-ui/icons/Add'
import NotificationsIcon from '@material-ui/icons/NotificationsNoneOutlined';
import useWindowDimensions from 'hooks/useWindowDimensions'

const Logo = () => (
  <Box>
    <Image src="/static/logo-mobile.svg" alt="" sx={{ width: [32, 0], display: 'block', mr: 4 }} />
    <Image src="/static/logo.svg" alt="" sx={{ width: [0, 135], display: 'block', mr: 4 }} />
  </Box>
)

const Header = ({ onNavOpenClick, onNavCloseClick, onAsideCloseClick, navVisible, asideVisible }) => {
  const { width } = useWindowDimensions();

  return (
    <Flex sx={{
      position: 'fixed',
      zIndex: 10,
      top: 0,
      width: '100%',
      justifyContent: 'space-between',
      px: 3,
      height: 54,
      bg: 'primary'
    }}>
      <Flex alignItems="center" width={['auto', 204, 300]}>
        <Box mr={3}>
          {asideVisible && width < 1024 ? (
            <KeyboardArrowLeftIcon style={{ display: 'block', fontSize: 30 }} onClick={onAsideCloseClick} />
          ) : navVisible ? (
            <MenuOpenIcon style={{ display: 'block', fontSize: 30 }} onClick={onNavCloseClick} />
          ) : (
            <MenuIcon style={{ display: 'block', fontSize: 30 }} onClick={onNavOpenClick} />
          )}
        </Box>
        <Logo />
      </Flex>
      <Flex sx={{ display: ['none', 'flex'], alignItems: 'center', mx: 'auto' }}>
        <Input
          placeholder="Buscar"
          sx={{
            display: ['none', 'block'],
            width: ['100%', 400],
            color: 'white',
            borderRadius: 3,
            border: 'none',
            outline: 'none',
            bg: `rgba(255, 255, 255, 0.1)`,
            '&::placeholder': {
              color: `rgba(255, 255, 255, 0.6)`
            }
          }}
        />
      </Flex>
      <Flex alignItems="center" justifyContent="flex-end" width={['auto', 204, 300]}>
        <Box ml={3}>
          <AddIcon style={{ display: 'block', fontSize: 30 }} />
        </Box>
        <Box ml={3}>
          <NotificationsIcon style={{ display: 'block', fontSize: 30 }} />
        </Box>
        <Image src="/static/avatar-1.jpeg" alt="" width={40} sx={{ display: 'block', ml: 3, borderRadius: 40 }} />
      </Flex>
    </Flex>
  )
}

export default Header