import { Text } from 'rebass/styled-components'

const Pre = (props) =>
  <Text
    {...props}
    as='pre'
    sx={{
      fontFamily: 'monospace',
      my: 3,
      p: 2,
      color: 'primary',
      bg: 'background.secondary',
      overflowX: 'auto'
    }}
  >
    {JSON.stringify(props.data, null, 2)}
  </Text>

export default Pre