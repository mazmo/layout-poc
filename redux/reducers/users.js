import { USER_AUTH } from 'redux/types/users'

const usersReducer = (state = {}, action) => {
  switch (action.type) {
    case USER_AUTH:
      return Object.assign({}, state, {
        current: action.payload
      })

    default:
      return state
  }
}

export default usersReducer