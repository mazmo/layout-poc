import { USER_AUTH } from 'redux/types/users'

export const userAuth = user => ({
  type: USER_AUTH,
  payload: user
})