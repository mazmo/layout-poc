import { useMemo } from 'react'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'

import usersReducer from 'redux/reducers/users'
import { USER_AUTH } from 'redux/types/users'

let store

const initialState = {
  users: {
    current: null
  }
}

const reducers = combineReducers({
  users: usersReducer
})

const logger = createLogger({
  predicate: (getState, action) => action.type !== USER_AUTH,
  collapsed: true
})

const middlewares = [
  thunk,
  process.env.NODE_ENV === 'development' && logger,
].filter(Boolean)

const initStore = (preloadedState = initialState) => (
  createStore(
    reducers,
    preloadedState,
    composeWithDevTools(applyMiddleware(...middlewares))
  )
)

export const initializeStore = (preloadedState) => {
  let _store = store ?? initStore(preloadedState)

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = initStore({
      ...store.getState(),
      ...preloadedState,
    })
    // Reset the current store
    store = undefined
  }

  // For SSG and SSR always create a new store
  if (typeof window === 'undefined') return _store
  // Create the store once in the client
  if (!store) store = _store

  return _store
}

export const useStore = (initialState) => {
  const store = useMemo(() => initializeStore(initialState), [initialState])
  return store
}